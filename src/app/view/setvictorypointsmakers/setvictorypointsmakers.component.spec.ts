import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetvictorypointsmakersComponent } from './setvictorypointsmakers.component';

describe('SetvictorypointsmakersComponent', () => {
  let component: SetvictorypointsmakersComponent;
  let fixture: ComponentFixture<SetvictorypointsmakersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetvictorypointsmakersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SetvictorypointsmakersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
