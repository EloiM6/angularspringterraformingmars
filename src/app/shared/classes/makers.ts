export interface Makers {
  idmaker: number,
  name: string,
  maxNeighbours: number,
  typeMaker: string,
  corporation: number
}
