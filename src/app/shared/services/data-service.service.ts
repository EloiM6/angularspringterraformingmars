import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {Torn} from "../classes/torn";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private dataSubject = new BehaviorSubject<Torn>({corporation:1,dices:[0,0,0,0,0,0]});
  public data$ = this.dataSubject.asObservable();

  updateData(newData: Torn) {
    this.dataSubject.next(newData);
  }
  constructor() { }
}
