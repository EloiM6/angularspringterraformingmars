import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Torn} from "../classes/torn";

@Injectable({
  providedIn: 'root'
})
export class ConnectService {

  REST_API: string = 'http://localhost:9000';
  constructor(private httpclient : HttpClient) {}
  public getAlive(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/`);
  }

  public getDices(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/rollingDices`);
  }
  public postResolveDices(dices:Torn): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/resolveDices`,dices);
  }
  public getIsEndGame(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/isEndGame`);
  }
  public getMakersByTypeWithoutCorporation(typeMakers: String): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/getMakersByTypeWithoutCorporation?typeMakers=`+ typeMakers );
  }

  public getMakersByTypeWithCorporation(typeMakers: String, idcorporation:number): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/getMakersByTypeWithCorporation?typeMakers=`+
      typeMakers + `&idCorporation=` + idcorporation);
  }

  public getCorporationsWithPlayer(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/getCorporationsWithPlayer`);
  }

  public postSetVictoryPointsMakers(): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/setVictoryPointsMakers`,"");
  }

  public postSetVictoryPointsWinners(): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/setVictoryPointsWinners`,"");
  }

  public postSetWinnerGame(): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/setWinnerGame`,"");
  }
}
