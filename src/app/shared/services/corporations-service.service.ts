import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {Torn} from "../classes/torn";
import {Corporations} from "../classes/corporations";

@Injectable({
  providedIn: 'root'
})
export class CorporationsService {
  private dataSubject = new BehaviorSubject<Corporations>({idcorporations:[0,0,0,0]});
  public data$ = this.dataSubject.asObservable();

  updateData(newData: Corporations) {
    this.dataSubject.next(newData);
  }
  constructor() { }
}
