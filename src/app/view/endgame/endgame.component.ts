import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";

@Component({
  selector: 'app-endgame',
  templateUrl: './endgame.component.html',
  styleUrls: ['./endgame.component.css']
})
export class EndgameComponent {
  message!:string;
  constructor(private connectbd: ConnectService){};
  resposta() {
    this.connectbd.getIsEndGame().subscribe(res => {
      this.message = res.estat;

    });
  }
}
