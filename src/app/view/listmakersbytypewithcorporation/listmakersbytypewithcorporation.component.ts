import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";
import {CorporationsService} from "../../shared/services/corporations-service.service";
import {Corporations} from "../../shared/classes/corporations";

@Component({
  selector: 'app-listmakersbytypewithcorporation',
  templateUrl: './listmakersbytypewithcorporation.component.html',
  styleUrls: ['./listmakersbytypewithcorporation.component.css']
})
export class ListmakersbytypewithcorporationComponent {
  typesMakers = ['OCEAN', 'CITY', 'JUNGLE'];
  selectedTypeMakers: string = '';
  message!: string;
  corporations !: Corporations;
  constructor(private connectbd: ConnectService, private corps : CorporationsService){};
  ngOnInit() {
    this.corps.data$.subscribe((data) => {
      this.corporations = data;
    });
  }
  resposta() {
    let corporation:number = this.corporations.idcorporations[Math.floor(Math.random() * 4)];
    this.connectbd.getMakersByTypeWithCorporation(this.selectedTypeMakers, corporation).subscribe(res => {
      this.message="La corporació és " + corporation;
      if (res.length > 0){
        for(let i = 0; i < res.length; i++){
          this.message += "<p>Id: " + res[i].idmaker + ", name: " + res[i].name + "<p/>";
        }
      } else {
        this.message += "<p>No hi ha elements.</p>";
      }

    });
  }
}
