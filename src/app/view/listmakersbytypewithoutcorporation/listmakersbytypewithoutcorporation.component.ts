import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";

@Component({
  selector: 'app-listmakersbytypewithoutcorporation',
  templateUrl: './listmakersbytypewithoutcorporation.component.html',
  styleUrls: ['./listmakersbytypewithoutcorporation.component.css']
})
export class ListmakersbytypewithoutcorporationComponent {
  typesMakers = ['OCEAN', 'CITY', 'JUNGLE'];
  selectedTypeMakers: string = '';
  message!: string;
  constructor(private connectbd: ConnectService){};
  resposta() {
    this.connectbd.getMakersByTypeWithoutCorporation(this.selectedTypeMakers).subscribe(res => {
      this.message="";
      if (res.length > 0){
        for(let i = 0; i < res.length; i++){
          this.message += "<p>Id: " + res[i].idmaker + ", name: " + res[i].name + "<p/>";
        }
      } else {
        this.message = "<p>No hi ha elements.</p>";
      }

    });
  }
}
