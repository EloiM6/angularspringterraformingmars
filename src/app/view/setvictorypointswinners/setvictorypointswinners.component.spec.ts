import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetvictorypointswinnersComponent } from './setvictorypointswinners.component';

describe('SetvictorypointswinnersComponent', () => {
  let component: SetvictorypointswinnersComponent;
  let fixture: ComponentFixture<SetvictorypointswinnersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetvictorypointswinnersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SetvictorypointswinnersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
