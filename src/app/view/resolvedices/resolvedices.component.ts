import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";
import {DataService} from "../../shared/services/data-service.service";
import {Torn} from "../../shared/classes/torn";
import {CorporationsService} from "../../shared/services/corporations-service.service";
import {Corporations} from "../../shared/classes/corporations";

@Component({
  selector: 'app-resolvedices',
  templateUrl: './resolvedices.component.html',
  styleUrls: ['./resolvedices.component.css']
})
export class ResolvedicesComponent {
  message!:string;
  message2!:string;
  dices!: Torn;
  corporations !: Corporations;
  constructor(private connectbd: ConnectService, private dataService: DataService, private corps : CorporationsService){};
  ngOnInit() {
    this.dataService.data$.subscribe((data) => {
      this.dices = data;
    });
    this.corps.data$.subscribe((data) => {
      this.corporations = data;
    });
  }
  resposta() {
    let newCorp:number = this.corporations.idcorporations[Math.floor(Math.random() * 4)];
    this.dices.corporation = newCorp;
    this.message2 = "La corporació és " + newCorp + ".";
    this.dataService.updateData({corporation:this.dices.corporation, dices: this.dices.dices});
    this.connectbd.postResolveDices(this.dices).subscribe(res => {
      this.message = res.estat;

    });
  }
}
