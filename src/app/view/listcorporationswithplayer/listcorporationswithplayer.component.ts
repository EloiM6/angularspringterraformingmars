import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";
import {CorporationsService} from "../../shared/services/corporations-service.service";
import {Corporations} from "../../shared/classes/corporations";

@Component({
  selector: 'app-listcorporationswithplayer',
  templateUrl: './listcorporationswithplayer.component.html',
  styleUrls: ['./listcorporationswithplayer.component.css']
})
export class ListcorporationswithplayerComponent {
  message!:string;
  corporations !: Corporations;
  ngOnInit() {
    this.corps.data$.subscribe((data) => {
      this.corporations = data;
    });
  }
  constructor(private connectbd: ConnectService, private corps : CorporationsService){};
  resposta() {
    this.connectbd.getCorporationsWithPlayer().subscribe(res => {
      this.message = "";

      if (res.length > 0){
        for(let i = 0; i < res.length; i++){
          this.message += "<p>Id: " + res[i].idcorporation + ", name: " + res[i].nom
            + ", player: " + res[i].player.name + "<p/>";
          this.corporations.idcorporations[i] = res[i].idcorporation;
        }
        this.corps.updateData(this.corporations);
      } else {
        this.message = "<p>No hi ha elements.</p>";
      }
    });
  }
}
