import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListmakersbytypewithcorporationComponent } from './listmakersbytypewithcorporation.component';

describe('ListmakersbytypewithcorporationComponent', () => {
  let component: ListmakersbytypewithcorporationComponent;
  let fixture: ComponentFixture<ListmakersbytypewithcorporationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListmakersbytypewithcorporationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListmakersbytypewithcorporationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
