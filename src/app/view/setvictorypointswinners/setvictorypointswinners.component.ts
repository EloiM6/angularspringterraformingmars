import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";

@Component({
  selector: 'app-setvictorypointswinners',
  templateUrl: './setvictorypointswinners.component.html',
  styleUrls: ['./setvictorypointswinners.component.css']
})
export class SetvictorypointswinnersComponent {
  message!:string;
  constructor(private connectbd: ConnectService){};
  resposta() {
    this.connectbd.postSetVictoryPointsWinners().subscribe(res => {
      this.message = res.estat;

    });
  }
}
