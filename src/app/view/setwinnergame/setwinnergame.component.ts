import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";

@Component({
  selector: 'app-setwinnergame',
  templateUrl: './setwinnergame.component.html',
  styleUrls: ['./setwinnergame.component.css']
})
export class SetwinnergameComponent {
  message!:string;
  constructor(private connectbd: ConnectService){};
  resposta() {
    this.connectbd.postSetWinnerGame().subscribe(res => {
      this.message = res.estat;

    });
  }
}
