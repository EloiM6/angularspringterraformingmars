import { TestBed } from '@angular/core/testing';

import { CorporationsServiceService } from './corporations-service.service';

describe('CorporationsServiceService', () => {
  let service: CorporationsServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CorporationsServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
