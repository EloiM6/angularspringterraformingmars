import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";

@Component({
  selector: 'app-setvictorypointsmakers',
  templateUrl: './setvictorypointsmakers.component.html',
  styleUrls: ['./setvictorypointsmakers.component.css']
})
export class SetvictorypointsmakersComponent {
  message!:string;
  constructor(private connectbd: ConnectService){};
  resposta() {
    this.connectbd.postSetVictoryPointsMakers().subscribe(res => {
      this.message = res.estat;

    });
  }
}
